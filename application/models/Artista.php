<?php
   class Artista extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("artista",$datos);
     }
     //Funcion que consulta todos los artistas de la bdd
     public function obtenerTodos(){
        $artistas=$this->db->get("artista");
        if ($artistas->num_rows()>0) {
          return $artistas;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un artista se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_art",$id);
        return $this->db->delete("artista");
     }
     //Consultando el artista por su id
     public function obtenerPorId($id){
        $this->db->where("id_art",$id);
        $artista=$this->db->get("artista");
        if($artista->num_rows()>0){
          return $artista->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de artistas
     public function actualizar($id,$datos){
       $this->db->where("id_art",$id);
       return $this->db->update("artista",$datos);
     }

   }//Cierre de la clase (No borrar)














//
