<?php
   class Lugar extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("lugar",$datos);
     }
     //Funcion que consulta todos los lugares de la bdd
     public function obtenerTodos(){
        $lugares=$this->db->get("lugar");
        if ($lugares->num_rows()>0) {
          return $lugares;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un lugar se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_lug",$id);
        return $this->db->delete("lugar");
     }
     //Consultando el lugar por su id
     public function obtenerPorId($id){
        $this->db->where("id_lug",$id);
        $lugar=$this->db->get("lugar");
        if($lugar->num_rows()>0){
          return $lugar->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de lugares
     public function actualizar($id,$datos){
       $this->db->where("id_lug",$id);
       return $this->db->update("lugar",$datos);
     }

   }//Cierre de la clase (No borrar)














//
