<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function insertar($datos){
        return $this->db->insert("cliente",$datos);
    }
    function obtenerTodos(){
      $listadoClientes=
      $this->db->get("cliente");
      if($listadoClientes->num_rows()>0){
      return $listadoClientes->result();
      }else {
      return false;
      }
    }
    function borrar($id_cli){
      $this->db->where("id_cli",$id_cli);
      if ($this->db->delete("cliente")) {
        return true;
      } else {
        return false;
      }
    }
    function obtenerPorId($id_cli){
      $this->db->where("id_cli",$id_cli);
      $cliente=$this->db->get("cliente");
      if ($cliente->num_rows()>0) {
        return $cliente->row();
      }
      return false;
    }
    function actualizar($id_cli,$datos){
      $this->db->where("id_cli",$id_cli);
      return $this->db->update('cliente',$datos);
    }
  }//Cierre de la clase

 ?>
