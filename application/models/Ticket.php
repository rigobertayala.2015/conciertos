<?php
   class Ticket extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
       return $this->db->insert('ticket',$datos);
     }

     public function obtenerTodos() {
      $this->db->select('ticket.id_tic, artista.nombre_art AS nombre_artista, lugar.nombre_lug AS nombre_lugar, concierto.fecha_con AS fecha_concierto, concierto.hora_con, ticket.precio_tic, ticket.area_tic, ticket.num_as_tic');
      $this->db->join("concierto", "concierto.id_con = ticket.fk_id_con");
      $this->db->join("artista", "artista.id_art = concierto.fk_id_art");
      $this->db->join("lugar", "lugar.id_lug = concierto.fk_id_lug");
      $tickets = $this->db->get("ticket");
      if ($tickets->num_rows() > 0) {
          return $tickets;
      }
      return false;
  }
  
  
  

  public function obtenerPorId($id) {
    $this->db->select('ticket.id_tic, artista.nombre_art AS nombre_artista, lugar.nombre_lug AS nombre_lugar, concierto.fecha_con AS fecha_concierto, concierto.hora_con, ticket.precio_tic, ticket.area_tic, ticket.num_as_tic');
    $this->db->join("concierto", "concierto.id_con = ticket.fk_id_con");
    $this->db->join("artista", "artista.id_art = concierto.fk_id_art");
    $this->db->join("lugar", "lugar.id_lug = concierto.fk_id_lug");
    $this->db->where("ticket.id_tic", $id);
    $ticket = $this->db->get("ticket");
    if ($ticket->num_rows() > 0) {
        return $ticket->row();
    }
    return false;
}



     public function actualizar($id_tic,$datos){
       $this->db->where("id_tic",$id_tic);
       return $this->db->update('ticket',$datos);
     }

     public function eliminar($id_tic){
      $this->db->where('id_tic', $id_tic);
      return $this->db->delete('ticket');
  }
  


}//Cierre de la clase
