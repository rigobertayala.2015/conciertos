<?php
   class Concierto extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
       return $this->db->insert('concierto',$datos);
     }

     public function obtenerTodos(){
       $this->db->join("lugar",
       "lugar.id_lug=concierto.fk_id_lug");
       $this->db->join("artista",
       "artista.id_art=concierto.fk_id_art");
       $conciertos=$this->db->get("concierto");
       if($conciertos->num_rows()>0){
         return $conciertos;
       }
       return false;
     }

     public function obtenerPorId($id){
      $this->db->join("lugar",
      "lugar.id_lug=concierto.fk_id_lug");
      $this->db->join("artista",
      "artista.id_art=concierto.fk_id_art");
       $this->db->where("id_con",$id);
       $concierto=$this->db->get("concierto");
       if($concierto->num_rows()>0){
         return $concierto->row();
       }
       return false;

     }


     public function actualizar($id_con,$datos){
       $this->db->where("id_con",$id_con);
       return $this->db->update('concierto',$datos);
     }

     public function eliminar($id_con){
      $this->db->where('id_con', $id_con);
      return $this->db->delete('concierto');
  }
  


   }//Cierre de la clase
