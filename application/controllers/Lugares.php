<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lugares extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("lugar");
	}
	//renderiza la vista index de lugares
	public function index()
	{
		$data["listadoLugares"]=
		$this->lugar->obtenerTodos();
		$this->load->view('header');
		$this->load->view('lugares/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de lugares
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('lugares/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarLugar(){
		$datosNuevoLugar=array(
			"nombre_lug"=>$this->input->post('nombre_lug'),
			"direccion_lug"=>$this->input->post('direccion_lug')
		);
		if($this->lugar->insertar($datosNuevoLugar)){
				$this->session
				->set_flashdata('confirmacion',
			 'Lugar insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('lugares/index');
	}
	//funcion para eliminar lugares
	public function borrar($id_lug){
		if ($this->lugar->eliminarPorId($id_lug)){
			$this->session
			->set_flashdata('confirmacion',
		 'Lugar ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('lugares/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["lugarEditar"]=
			$this->lugar->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("lugares/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosLugarEditado=array(
			"nombre_lug"=>$this->input->post('nombre_lug'),
			"direccion_lug"=>$this->input->post('direccion_lug')
		);
	 $id=$this->input->post("id_lug");
		if($this->lugar->actualizar($id,$datosLugarEditado)){
			redirect('lugares/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
