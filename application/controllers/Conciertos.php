<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conciertos extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("lugar");
    $this->load->model("artista");
		$this->load->model("concierto");
	}

  public function index(){
		$data["listadoConciertos"]=
		$this->concierto->obtenerTodos();
    $this->load->view("header");
    $this->load->view("conciertos/index",$data);
    $this->load->view("footer");
  }


  public function nuevo(){
		$data["listadoLugares"]=$this->lugar->obtenerTodos();
		$data["listadoArtistas"]=$this->artista->obtenerTodos();
    $this->load->view("header");
    $this->load->view("conciertos/nuevo",$data);
    $this->load->view("footer");
  }

	public function editar($id_con){
		$data["concierto"]=$this->concierto->obtenerPorId($id_con);
		$data["listadoLugares"]=$this->lugar->obtenerTodos();
		$data["listadoArtistas"]=$this->artista->obtenerTodos();
		$this->load->view("header");
		$this->load->view("conciertos/editar",$data);
		$this->load->view("footer");
	}

	public function guardarConcierto(){
		$datos=array(
			"fk_id_art"=>$this->input->post("fk_id_art"),
      		"fk_id_lug"=>$this->input->post("fk_id_lug"),
			"fecha_con"=>$this->input->post("fecha_con"),
			"hora_con"=>$this->input->post("hora_con"),
			"descripcion_con"=>$this->input->post("descripcion_con")
		);
		if($this->concierto->insertar($datos)){
				$this->session->set_flashdata("confirmacion",
			  "Concierto Insertado Exitosamente");
		}else{
			$this->session->set_flashdata("error",
			"Error al inserta, verifique e intente otra vez");
		}
		redirect("conciertos/index");
	}



	public function actualizarConcierto(){
		$datos=array(
			"fk_id_art"=>$this->input->post("fk_id_art"),
      		"fk_id_lug"=>$this->input->post("fk_id_lug"),
			"fecha_con"=>$this->input->post("fecha_con"),
			"hora_con"=>$this->input->post("hora_con"),
			"descripcion_con"=>$this->input->post("descripcion_con")
		);
		$id_con=$this->input->post("id_con");
		if($this->concierto->actualizar($id_con,$datos)){
				$this->session->set_flashdata("confirmacion",
			  "Concierto Actulizado Exitosamente");
				redirect("conciertos/index");
		}else{
			$this->session->set_flashdata("error",
			"Error al actualizar, verifique e intente otra vez");
			redirect("conciertos/editar/".$id_con);
		}

	}
	
	public function eliminar($id_con) {
		if ($this->concierto->eliminar($id_con)){
			$this->session
			->set_flashdata('confirmacion',
		 'Concierto ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('conciertos/index');
	}
	
	

}//Cierre de la clase (No borrar)






//
