<?php

class Clientes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('cliente');
  }
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('clientes/nuevo');
    $this->load->view('footer');
  }
  public function index(){
    $data['clientes']=$this->cliente->obtenerTodos();
    $this->load->view('header');
    $this->load->view('clientes/index',$data);
    $this->load->view('footer');
  }
  public function guardar(){
    $datosNuevocliente=array(
      "nombre_cli"=>$this->input->post('nombre_cli'),
      "apellido_cli"=>$this->input->post('apellido_cli'),
      "cedula_cli"=>$this->input->post('cedula_cli')
    );
    if($this->cliente->insertar($datosNuevocliente)){
      redirect('clientes/index');
    }else {
      echo "ERROR AL INSERTAR EL cliente";
    }
  }
  public function eliminar($id_cli){

     if ($this->cliente->borrar($id_cli)) {
     redirect('clientes/index');
   } else {
     echo "ERROR AL BORRAR LOS clientes";
   }
}
  public function editar($id_cli){
    $data["clienteEditar"]=$this->cliente->obtenerPorId($id_cli);
    $this->load->view('header');
    $this->load->view('clientes/actualizar',$data);
    $this->load->view('footer');
  }
  public function procesarActualizacion(){
    $datosEditados=array(
      "nombre_cli"=>$this->input->post('nombre_cli'),
      "apellido_cli"=>$this->input->post('apellido_cli'),
      "cedula_cli"=>$this->input->post('cedula_cli')
    );
    $id_cli=$this->input->post("id_cli");
    if ($this->cliente->actualizar($id_cli,$datosEditados)) {
      redirect('clientes/index');
    } else {
      echo "ERROR AL ACTUALIZAR LOS clientes";
    }
 }

}//NO borrar el cierre de la clase


 ?>
