<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("concierto");
		$this->load->model("ticket");
	}

  public function index(){
		$data["listadoTickets"]=
		$this->ticket->obtenerTodos();
    $this->load->view("header");
    $this->load->view("tickets/index",$data);
    $this->load->view("footer");
  }


  public function nuevo(){
		$data["listadoConciertos"]=$this->concierto->obtenerTodos();
    $this->load->view("header");
    $this->load->view("tickets/nuevo",$data);
    $this->load->view("footer");
  }

	public function editar($id_con){
		$data["ticket"]=$this->ticket->obtenerPorId($id_tic);
		$data["listadoConciertos"]=$this->concierto->obtenerTodos();
		$this->load->view("header");
		$this->load->view("tickets/editar",$data);
		$this->load->view("footer");
	}

	public function guardarTicket(){
		$datos=array(
			"precio_tic"=>$this->input->post("precio_tic"),
      "area_tic"=>$this->input->post("area_tic"),
			"num_as_tic"=>$this->input->post("num_as_tic"),
			"fk_id_con"=>$this->input->post("fk_id_con")
		);
		if($this->ticket->insertar($datos)){
				$this->session->set_flashdata("confirmacion",
			  "Ticket Insertado Exitosamente");
		}else{
			$this->session->set_flashdata("error",
			"Error al inserta, verifique e intente otra vez");
		}
		redirect("tickets/index");
	}



	public function actualizarTicket(){
		$datos=array(
			"precio_tic"=>$this->input->post("precio_tic"),
      "area_tic"=>$this->input->post("area_tic"),
			"num_as_tic"=>$this->input->post("num_as_tic"),
			"fk_id_con"=>$this->input->post("fk_id_con")
		);
		$id_tic=$this->input->post("id_tic");
		if($this->ticket->actualizar($id_tic,$datos)){
				$this->session->set_flashdata("confirmacion",
			  "Ticket Actulizado Exitosamente");
				redirect("tickets/index");
		}else{
			$this->session->set_flashdata("error",
			"Error al actualizar, verifique e intente otra vez");
			redirect("tickets/editar/".$id_tic);
		}

	}
	
	public function eliminar($id_tic) {
		if ($this->ticket->eliminar($id_tic)){
			$this->session
			->set_flashdata('confirmacion',
		 'Ticket ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('tickets/index');
	}

	public function verDetalles($id_tic) {
		$data['ticket'] = $this->ticket->obtenerPorId($id_tic);
		$this->load->view('tickets/detalle', $data);

	}
	
	

}//Cierre de la clase (No borrar)






//
