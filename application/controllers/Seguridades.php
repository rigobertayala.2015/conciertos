<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seguridades extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("usuario");
		$this->load->model("log1");
	}
  public function login(){
  	$this->load->view("seguridades/login");
  }
	public function validarUsuario(){
		$email=$this->input->post("email_usu");
		$password=$this->input->post("password_usu");
		$usuarioEncontrado=$this->usuario->obtenerPorEmailPassword($email,$password);
		if ($usuarioEncontrado){

			$this->session->set_userdata('conectad0',$usuarioEncontrado);

			$datosLogs=array(
				"fk_id_usu"=>$usuarioEncontrado->id_usu,
				"descripcion_log"=>"INGRESO"
				);
			$this->log1->insertar($datosLogs);

			$this->session->set_flashdata("confirmacion","Bienvenido al sistema");
			redirect("welcome/index");
		} else {

		$this->session->set_flashdata("error","Email o contraseña incorrectos");
		redirect("seguridades/login");
		}
 }
 
 public function logout() {
    // Cerrar la sesión del usuario
    $this->session->unset_userdata('conectad0'); // Eliminar la variable de sesión que indica que el usuario está conectado
    $this->session->sess_destroy(); // Destruir todas las variables de sesión

    // Redirigir a la página de inicio de sesión o a la página principal
    redirect('seguridades/login'); // Reemplaza 'seguridades/login' con la URL a la que deseas redirigir al usuario después de cerrar sesión
}

}//Cierre de la clase
