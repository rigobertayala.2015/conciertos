<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artistas extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("artista");
	}
	//renderiza la vista index de artistas
	public function index()
	{
		$data["listadoArtistas"]=
		$this->artista->obtenerTodos();
		$this->load->view('header');
		$this->load->view('artistas/index',$data);
		$this->load->view('footer');
	}
 //renderiza la vista nuevo de artistas
	public function nuevo()
	{
		$this->load->view('header');
		$this->load->view('artistas/nuevo');
		$this->load->view('footer');
	}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardarArtista(){
		$datosNuevoArtista=array(
			"nombre_art"=>$this->input->post('nombre_art')
		);
		if($this->artista->insertar($datosNuevoArtista)){
				$this->session
				->set_flashdata('confirmacion',
			 'Artista insertado exitosamente');
		}else{
			$this->session
			->set_flashdata('error',
		 'Error al insertar, verifique e intente de nuevo');
		}
		redirect('artistas/index');
	}
	//funcion para eliminar artista
	public function borrar($id_art){
		if ($this->artista->eliminarPorId($id_art)){
			$this->session
			->set_flashdata('confirmacion',
		 'Artista ELIMINADO exitosamente');
		} else {
			$this->session
			->set_flashdata('error',
		 'Error al ELIMINAR, verifique e intente de nuevo');
		}
		redirect('artistas/index');
	}
	//Funcion para renderizar el formulario de
	//actualizacion
	public function actualizar($id){
		  $data["artistaEditar"]=
			$this->artista->obtenerPorId($id);
			$this->load->view("header");
			$this->load->view("artistas/actualizar",$data);
			$this->load->view("footer");
	}

	public function procesarActualizacion(){
		$datosArtistaEditado=array(
			"nombre_art"=>$this->input->post('nombre_art')
		);
	 $id=$this->input->post("id_art");
		if($this->artista->actualizar($id,$datosArtistaEditado)){
			redirect('artistas/index');
		}else{
			echo "<h1>ERROR</h1>";
		}
	}

}// cierre de la clase (No borrar)




//
