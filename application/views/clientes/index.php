<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE CLIENTES</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('clientes/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>Agregar Cliente
  </a>
</center>
<br>
</div>
<br>
<?php if ($clientes): ?>
  <div style="margin: 0 120px;"> 
    <table class="table table-striped table-bordered table-hover" id="tbl_clientes">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>APELLIDO</th>
          <th>CÉDULA</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($clientes as $filaTemporal): ?>
          <tr>
            <td>
              <?php echo $filaTemporal->id_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal->nombre_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal->apellido_cli ?>
            </td>
            <td>
              <?php echo $filaTemporal->cedula_cli ?>
            </td>
            <td class="text-center">
              <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" title="Editar Cliente">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Editar
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>" title="Eliminar Cliente" onclick="return confirm('¿Estás seguro de eliminar de forma permanente?');" style="color: red;">
              <button type="submit" name="button" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i>
              Eliminar
              </button>
            </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1 class="text-center">No existen Clientes</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_clientes").DataTable();
</script>
