<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO CLIENTE</h1>
<form class="" id="frm_nuevo_cliente" action="<?php echo site_url(); ?>/clientes/guardar" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="nombre_cli">NOMBRE:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese el nombre del cliente" class="form-control" required name="nombre_cli" id="nombre_cli">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="apellido_cli">APELLIDO:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese el apellido del cliente" class="form-control" required name="apellido_cli" id="apellido_cli">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="cedula_cli">CÉDULA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese la cédula del cliente" class="form-control" required name="cedula_cli" id="cedula_cli">
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-primary">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_cliente").validate({
  rules:{
    nombre_cli:{
      required:true,
    },
    apellido_cli:{
      required:true,
    },
    cedula_cli:{
      required:true,
      digits:true,
      maxlength:10,
      minlength:10,
    }
  },
  messages:{
    nombre_cli:{
      required:"Por favor ingrese el nombre del cliente",
    },
    apellido_cli:{
      required:"Por favor ingrese el apellido del cliente",
    },
    cedula_cli:{
      required:"Por favor ingrese la cédula del cliente",
      digits:"Este campo solo admite números",
      maxlength:"La cédula debe tener 10 dígitos",
      minlength:"La cédula debe tener 10 dígitos"
    }
  }
});
</script>
