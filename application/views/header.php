<?php if (!$this->session->userdata('conectad0')): ?>
  <script type="text/javascript">
    window.location.href="<?php echo site_url('seguridades/login'); ?>"
  </script>
<?php endif; ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">

    <title>CONCERT</title>
    <!-- importacion de jquery -->
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
    <!-- IMPORTACION DE BOOSTRAP -->
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<body style="background:url('https://images.pexels.com/photos/1287083/pexels-photo-1287083.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1') no-repeat; background-size: cover ;">
    <body>
      
    <!-- Importacion de jquery validate -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <script type="text/javascript">
   jQuery.validator.addMethod("letras", function(value, element) {
     //return this.optional(element) || /^[a-z]+$/i.test(value);
     return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñóú ]*$/.test(value);

   }, "Este campo solo acepta letras");
 //importacion de toastr
 </script>
    <!-- //importacion de datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js">
    </script>
   <!-- Importación de SweetAlert2 -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<?php if ($this->session->flashdata('confirmacion')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'CONFIRMACIÓN', //titulo
            '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
            'success' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>


    <?php if ($this->session->flashdata('error')): ?>
      <script type="text/javascript">
        $(document).ready(function(){
          Swal.fire(
            'ERROR', //titulo
            '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
            'error' //Tipo de alerta
          )
        });
      </script>
    <?php endif; ?>
  </head>
  <body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo site_url(); ?>">CONCERT</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LOCATION <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('lugares/nuevo'); ?>">NEW</a></li>
              <li><a href="<?php echo site_url('lugares/index'); ?>">LIST</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CONCERTS <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('conciertos/nuevo'); ?>">NEW</a></li>
              <li><a href="<?php echo site_url('conciertos/index'); ?>">LIST</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTIST <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('artistas/nuevo'); ?>">NEW</a></li>
              <li><a href="<?php echo site_url('artistas/index'); ?>">LIST</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CUSTOMER <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('clientes/nuevo'); ?>">NEW</a></li>
              <li><a href="<?php echo site_url('clientes/index'); ?>">LIST</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="<?php echo site_url(); ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TICKET <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url(); ?>/tickets/nuevo">NEW</a></li>
              <li><a href="<?php echo site_url(); ?>/tickets/index">LIST</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <?php if ($this->session->userdata('conectad0')): ?>
            <li><a href="<?php echo site_url('seguridades/logout'); ?>">Log out</a></li>
          <?php endif; ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
