<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO LUGAR</h1>
<form class="" id="frm_nuevo_lugar" action="<?php echo site_url('lugares/guardarLugar'); ?>" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="nombre_lug">NOMBRE:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese el nombre del lugar" class="form-control" required name="nombre_lug" id="nombre_lug">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="direccion_lug">DIRECCIÓN:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese la dirección del lugar" class="form-control" required name="direccion_lug" id="direccion_lug">
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-primary">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/lugares/index" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban-circle"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_lugar").validate({
    rules:{
        nombre_lug:{
            required:true,
        },
        direccion_lug:{
            required:true,
        }
    },
    messages:{
        nombre_lug:{
            required:"Por favor ingrese el nombre del lugar",
        },
        direccion_lug:{
            required:"Por favor ingrese la dirección del lugar",
        }
    }
});
</script>
