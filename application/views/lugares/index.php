<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE LUGARES</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('lugares/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>Agregar Lugar</a>
</center>
<br>
</div>
<br>
<?php if ($listadoLugares): ?>
  <div style="margin: 0 120px;"> 
    <table class="table table-striped table-bordered table-hover" id="tbl_lugares">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>DIRECCIÓN</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($listadoLugares->result()
           as $lugarTemporal): ?>
          <tr>
            <td>
              <?php echo $lugarTemporal->id_lug ?>
            </td>
            <td>
              <?php echo $lugarTemporal->nombre_lug ?>
            </td>
            <td>
              <?php echo $lugarTemporal->direccion_lug ?>
            </td>
            <td class="text-center">
              <a href="<?php echo site_url(); ?>/lugares/actualizar/<?php echo $lugarTemporal->id_lug; ?>" title="Editar Lugar">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Editar
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/lugares/borrar/<?php echo $lugarTemporal->id_lug; ?>" title="Eliminar Lugar" onclick="return confirm('¿Estás seguro de eliminar de forma permanente?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Eliminar
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1>No existen Lugares</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_lugares").DataTable();
</script>
