<h1><i class=""></i> ACTUALIZAR LUGARES</h1>
<form class=""
id="frm_actualizar_lugar"
action="<?php echo site_url('lugares/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
    <div class="row">
      <input type="hidden" name="id_lug" id="id_lug" value="<?php echo $lugarEditar->id_lug; ?>">
      <div class="col-md-4">
          <label for="">NOMBRE:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del lugar"
          class="form-control"
          required
          name="nombre_lug" value="<?php echo $lugarEditar->nombre_lug; ?>"
          id="nombre_lug">
          </div>
    <br>
    <div class="col-md-4">
          <label for="">DIRECCIÓN:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese la dirección del lugar"
          class="form-control"
          required
          name="direccion_lug" value="<?php echo $lugarEditar->direccion_lug; ?>"
          id="direccion_lug">
          </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/lugares/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_lugares").validate({
   rules:{
     nombre_lug:{
       required:true
     },
     direccion_lug:{
       required:true
     }
   },
   messages:{
    nombre_lug:{
       required: "Por favor ingrese el nombre del lugar"
     },
        direccion_lug:{
        required: "Por favor ingrese la dirección del lugar"
        }
   }
 });

 </script>
