<!DOCTYPE html>
<html>
<head>
    <title>Detalles del Ticket</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background: #f0f0f0; /* Cambia el fondo a un color gris claro */
        }

        .outer-rect {
            display: flex;
            justify-content: center;
            align-items: center;
            border: 2px solid #000;
            border-radius: 20px; /* Valor positivo para esquinas circulares */
            padding: 20px;
            width: 75%; /* Ajusta el ancho del rectángulo más grande según tu preferencia */
        }

        .qrcode {
            width: 135px;
            height: 135px;
            margin-right: 10px;
        }

        .ticket {
            text-align: center;
            width: 100%;
            background-color: #c3c3c3;
            color: black;
            font-size: 24px;
        }

        .artist-name {
            font-size: 50px;
            margin-bottom: 10px;
        }

        .artist-info, .ticket-info {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-top: 10px;
        }

        .ticket-info p {
            flex: 1;
        }

        .place {
            font-size: 35px;
        }
    </style>
</head>
<body>
    <div class="outer-rect">
        <div class="qrcode" id="qrcode"></div>
        <div class="ticket">
            <p class="artist-name"><b><?php echo $ticket->nombre_artista; ?></b></p>
            <p class="place"><b>Lugar:</b> <?php echo $ticket->nombre_lugar; ?></p>
            <div class="ticket-info">
                <p><b>Fecha:</b> <?php echo $ticket->fecha_concierto; ?></p>
                <p><b>Hora:</b> <?php echo $ticket->hora_con; ?></p>
                <p><b>Precio:</b> $<?php echo $ticket->precio_tic; ?></p>
            </div>
            <div class="ticket-info">
                <p><b>Área:</b> <?php echo $ticket->area_tic; ?></p>
                <p><b>Número de Asiento:</b> <?php echo $ticket->num_as_tic; ?></p>
            </div>
        </div>
    </div>

    <script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
    <script>
        // Genera un valor aleatorio para el código QR
        const randomQRValue = Math.random().toString(36).substring(2, 10);

        // Crea un nuevo objeto QRCode y establece el valor del código QR
        const qrcode = new QRCode(document.getElementById("qrcode"), {
            text: randomQRValue,
            width: 128,
            height: 128
        });
    </script>
</body>
</html>
