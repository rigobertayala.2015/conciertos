<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO TICKET</h1>
<form id="frm_nuevo_ticket" action="<?php echo site_url('tickets/guardarTicket'); ?>" method="post" enctype="multipart/form-data">

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
                <b>CONCIERTO:</b> <br>
                <select class="form-control" name="fk_id_con" id="fk_id_con" required data-live-search="true">
                    <option value="">--Seleccione el Concierto--</option>
                    <?php if ($listadoConciertos): ?>
                        <?php foreach ($listadoConciertos->result() as $concierto): ?>
                            <option value="<?php echo $concierto->id_con; ?>">
                                <?php echo $concierto->nombre_art; ?>
                                |
                                <?php echo $concierto->nombre_lug; ?>
                                |
                                <?php echo $concierto->fecha_con; ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <br>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_con">PRECIO:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="number" class="form-control" required name="precio_tic" id="precio_tic">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="hora_con">ÁREA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" class="form-control" required name="area_tic" id="area_tic">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="descripcion_con">NÚMERO DE ASIENTO:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="number" class="form-control" required name="num_as_tic" id="num_as_tic">
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-primary">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url('tickets/index'); ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
   $('#fk_id_con').val("<?php echo $ticket->fk_id_con; ?>");
   $('#fk_id_con').selectpicker();
</script>

<!-- <script type="text/javascript">
    $("#frm_nuevo_concierto").validate({
        rules: {
            fk_id_art: {
                required: true,
            },
            fecha_con: {
                required: true,
            },
            hora_con: {
                required: true,
            },
            fk_id_lug: {
                required: true,
            },
            descripcion_con: {
                required: true,
            }
        },
        messages: {
            fk_id_art: {
                required: "Por favor seleccione el artista",
            },
            fecha_con: {
                required: "Por favor ingrese una fecha",
            },
            hora_con: {
                required: "Por favor ingrese una hora",
            },
            fk_id_lug: {
                required: "Por favor seleccione el lugar",
            },
            descripcion_con: {
                required: "Por favor ingrese una breve descripción",
            }
        }
    });
</script> -->
