<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE TICKETS</h1>
  </div>
</div>
<center>
    <a href="<?php echo site_url('tickets/nuevo'); ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>Agregar Ticket</a>
</center>
<br>
</div>
<br>
<?php if ($listadoTickets): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_tickets">
    <thead>
      <tr>
        <th>ID</th>
        <th>ARTISTA</th>
        <th>LUGAR</th>
        <th>FECHA</th>
        <th>HORA</th>
        <th>PRECIO</th>
        <th>ÁREA</th>
        <th>NÚMERO DE ASIENTO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoTickets->result() as $ticket): ?>
        <tr>
          <td>
            <?php echo $ticket->id_tic ?>
          </td>
          <td>
            <?php echo $ticket->nombre_artista ?>
          </td>
          <td>
            <?php echo $ticket->nombre_lugar ?>
          </td>
          <td>
            <?php echo $ticket->fecha_concierto ?>
          </td>
          <td>
            <?php echo $ticket->hora_con ?>
          </td>
          <td>
            <?php echo $ticket->precio_tic ?>
          </td>
          <td>
            <?php echo $ticket->area_tic ?>
          </td>
          <td>
            <?php echo $ticket->num_as_tic ?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url('tickets/editar').'/'.$ticket->id_tic; ?>" class="btn btn-warning">
              <i class="glyphicon glyphicon-edit"></i> Editar
            </a>
            <a href="<?php echo site_url('tickets/eliminar').'/'.$ticket->id_tic; ?>" class="btn btn-danger">
              <i class="glyphicon glyphicon-trash"></i> Eliminar
            </a>
            <a href="<?php echo site_url('tickets/verDetalles/' . $ticket->id_tic); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-eye-open"></i> Ver
            </a>
            <button class="btn btn-primary" onclick="printTicket(<?php echo $ticket->id_tic; ?>)">
              <i class="glyphicon glyphicon-print"></i> Imprimir
            </button>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h1 class="text-center">No existen Tickets </h1>
<?php endif; ?>
  


<script type="text/javascript">
  $("#tbl_tickets").DataTable();
</script>

<script>
  function printTicket(ticketId) {
  // Abre una nueva ventana o pestaña del navegador
  var printWindow = window.open("<?php echo site_url('tickets/verDetalles/'); ?>" + ticketId, "_blank");

  // Espera a que se cargue la página de impresión y luego imprime
  printWindow.onload = function() {
    printWindow.print();
  };
}
</script>
