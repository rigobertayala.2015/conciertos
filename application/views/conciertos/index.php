<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE CONCIERTOS</h1>
  </div>
</div>
<center>
    <a href="<?php echo site_url('conciertos/nuevo'); ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>Agregar Concierto</a>
</center>
<br>
</div>
<br>
<?php if ($listadoConciertos): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_conciertos" >
    <thead>
      <tr>
        <th>ID</th>
        <th>ARTISTA</th>
        <th>FECHA</th>
        <th>HORA</th>
        <th>LUGAR</th>
        <th>DESCRIPCIÓN</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoConciertos->result() as $concierto): ?>
        <tr>
          <td>
            <?php echo $concierto->id_con ?>
          </td>
          <td>
            <?php echo $concierto->nombre_art ?>
          </td>
          <td>
            <?php echo $concierto->fecha_con ?>
          </td>
          <td>
            <?php echo $concierto->hora_con ?>
          </td>
          <td>
            <?php echo $concierto->nombre_lug ?>
          </td>
          <td>
            <?php echo $concierto->descripcion_con ?>
          </td>
          <td class="text-center">
                    <a href="<?php echo site_url('conciertos/editar').'/'.$concierto->id_con; ?>" class="btn btn-warning">
                      <i class="glyphicon glyphicon-edit"></i>
                      Editar
                    </a>
                    <a href="<?php echo site_url('conciertos/eliminar').'/'.$concierto->id_con; ?>" class="btn btn-danger">
                        <i class="glyphicon glyphicon-trash"></i>
                        Eliminar
                    </a>

                </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1 class="text-center">No existen Conciertos </h1>
<?php endif; ?>


<script type="text/javascript">
  $("#tbl_conciertos").DataTable();
</script>
