<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO CONCIERTO</h1>
<form id="frm_nuevo_concierto" action="<?php echo site_url('conciertos/guardarConcierto'); ?>" method="post" enctype="multipart/form-data">

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
                <b>ARTISTA:</b> <br>
                <select class="form-control" name="fk_id_art" id="fk_id_art" required data-live-search="true">
                    <option value="">--Seleccione el Artista--</option>
                    <?php if ($listadoArtistas): ?>
                        <?php foreach ($listadoArtistas->result() as $artistaTemporal): ?>
                            <option value="<?php echo $artistaTemporal->id_art; ?>">
                                <?php echo $artistaTemporal->nombre_art; ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <br>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_con">FECHA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="date" class="form-control" required name="fecha_con" id="fecha_con">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="hora_con">HORA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="time" class="form-control" required name="hora_con" id="hora_con">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <b>LUGAR:</b> <br>
                <select class="form-control" name="fk_id_lug" id="fk_id_lug" required data-live-search="true">
                    <option value="">--Seleccione el Lugar--</option>
                    <?php if ($listadoLugares): ?>
                        <?php foreach ($listadoLugares->result() as $lugarTemporal): ?>
                            <option value="<?php echo $lugarTemporal->id_lug; ?>">
                                <?php echo $lugarTemporal->nombre_lug; ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <br>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="descripcion_con">DESCRIPCIÓN:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" class="form-control" required name="descripcion_con" id="descripcion_con">
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-primary">
                <i class="glyphicon glyphicon-check"></i> Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url('conciertos/index'); ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-ban"></i> Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
   $('#fk_id_art').val("<?php echo $concierto->fk_id_art; ?>");
   $('#fk_id_lug').val("<?php echo $concierto->fk_id_lug; ?>");
   $('#fk_id_art').selectpicker();
   $('#fk_id_lug').selectpicker();
</script>

<script type="text/javascript">
    $("#frm_nuevo_concierto").validate({
        rules: {
            fk_id_art: {
                required: true,
            },
            fecha_con: {
                required: true,
            },
            hora_con: {
                required: true,
            },
            fk_id_lug: {
                required: true,
            },
            descripcion_con: {
                required: true,
            }
        },
        messages: {
            fk_id_art: {
                required: "Por favor seleccione el artista",
            },
            fecha_con: {
                required: "Por favor ingrese una fecha",
            },
            hora_con: {
                required: "Por favor ingrese una hora",
            },
            fk_id_lug: {
                required: "Por favor seleccione el lugar",
            },
            descripcion_con: {
                required: "Por favor ingrese una breve descripción",
            }
        }
    });
</script>
