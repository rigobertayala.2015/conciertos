<script type="text/javascript">
  $("#menu-conciertos").addClass('active');
</script>

<div class="container">
  <center>
    <h3><b>Actualizar Concierto</b></h3>
  </center>
  <br>
  <form class=""
  action="<?php echo site_url('conciertos/actualizarConcierto'); ?>"
  method="post">

    <input type="hidden" name="id_con" value="<?php echo $concierto->id_con; ?>">

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
            <b>ARTISTA:</b> <br>
                <select class="form-control" name="fk_id_art"
                id="fk_id_art" required data-live-search="true">
                    <option value="">--Seleccione el Artista--</option>
                    <?php if ($listadoArtistas): ?>
                        <?php foreach ($listadoArtistas->result()
                        as $artistaTemporal): ?>
                        <option value="<?php echo $artistaTemporal->id_art; ?>">
                            <?php echo $artistaTemporal->nombre_art; ?>
                        </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <br>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="fecha_con">FECHA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input class="form-control" type="date" name="fecha_con" id="fecha_con" value="<?php echo substr($concierto->fecha_con,0,10); ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="hora_con">HORA:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="time" placeholder="Ingrese la hora del concierto" class="form-control" required name="hora_con" id="hora_con">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <b>LUGAR:</b> <br>
                <select class="form-control" name="fk_id_lug"
                id="fk_id_lug" required data-live-search="true">
                    <option value="">--Seleccione el Lugar--</option>
                    <?php if ($listadoLugares): ?>
                        <?php foreach ($listadoLugares->result()
                        as $lugarTemporal): ?>
                        <option value="<?php echo $lugarTemporal->id_lug; ?>">
                            <?php echo $lugarTemporal->nombre_lug; ?>
                        </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <br>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="descripcion_con">DESCRIPCIÓN:
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <input type="text" placeholder="Ingrese una breve descripción del concierto" class="form-control" required name="descripcion_con" id="descripcion_con">
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-success">
            Guardar
            </button>
            <a href="<?php echo site_url('conciertos/index'); ?>"
            class="btn btn-danger">
            Cancelar
            </a>
        </div>
    </div>
  </form>

</div>




<script type="text/javascript">
   // Seleccionar valores en los campos relacionados con conciertos
   $('#fk_id_art').val("<?php echo $concierto->fk_id_art; ?>");
   $('#fk_id_lug').val("<?php echo $concierto->fk_id_lug; ?>");
   $('#fecha_con').val("<?php echo $concierto->fecha_con; ?>");
   $('#hora_con').val("<?php echo $concierto->hora_con; ?>");
   $('#descripcion_con').val("<?php echo $concierto->descripcion_con; ?>");

   // Inicializar los selectpickers
   $('#fk_id_art').selectpicker();
   $('#fk_id_lug').selectpicker();
</script>



<!--  -->
