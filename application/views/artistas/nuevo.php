<h1 class="text-center"><i class="glyphicon glyphicon-plus"></i> NUEVO ARTISTA</h1>
<form class=""
id="frm_nuevo_artista"
action="<?php echo site_url(); ?>/artistas/guardarArtista"
method="post"
enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-2">

      </div>

      <div class="col-md-8">
          <label for="">NOMBRE:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del artista"
          class="form-control"
          required
          name="nombre_art" value=""
          id="nombre_art">
      </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/artistas/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

<script type="text/javascript">
$("#frm_nuevo_artista").validate({
  rules:{
    nombre_art:{
      required:true,
    }
  },
  messages:{
    nombre_art:{
      required: "Ingrese el nombre del artista",
    }
  }
});
</script>
