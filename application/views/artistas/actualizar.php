<h1><i class=""></i> ACTUALIZAR ARTISTAS</h1>
<form class=""
id="frm_actualizar_artista"
action="<?php echo site_url('artistas/procesarActualizacion'); ?>"
method="post"
enctype="multipart/form-data">
    <div class="row">
      <input type="hidden" name="id_art" id="id_art" value="<?php echo $artistaEditar->id_art; ?>">
      <div class="col-md-4">
          <label for="">NOMBRE:
            <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del artista"
          class="form-control"
          required
          name="nombre_art" value="<?php echo $artistaEditar->nombre_art; ?>"
          id="nombre_art">
          </div>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
          <br><br>
            <button type="submit" name="button"
            class="btn btn-primary">
            <i class="glyphicon glyphicon-check"></i>
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/artistas/index"
              class="btn btn-danger">
              <i class="glyphicon glyphicon-ban-circle"></i>
              Cancelar
            </a>
        </div>
    </div>
</form>

 <script type="text/javascript">
 $("#frm_actualizar_artista").validate({
   rules:{
     nombre_art:{
       required:true
     }
   },
   messages:{
    nombre_art:{
       required: "Por favor ingrese el nombre del artista"
     }
   }
 });

 </script>
