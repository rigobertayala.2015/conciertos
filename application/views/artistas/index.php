<div class="row">
  <div class="col-md-12">
    <h1 class="text-center"><i class=""></i> LISTADO DE ARTISTAS</h1>
  </div>
</div>
<center>
  <a href="<?php echo site_url('artistas/nuevo'); ?>" class="btn btn-success">
    <i class="glyphicon glyphicon-plus"></i>Agregar Artista
  </a>
</center>
<br>
</div>
<br>
<?php if ($listadoArtistas): ?>
  <div style="margin: 0 120px;"> 
    <table class="table table-striped table-bordered table-hover" id="tbl_artistas">
      <thead>
        <tr>
          <th>ID</th>
          <th>NOMBRE</th>
          <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($listadoArtistas->result() as $artistaTemporal): ?>
          <tr>
            <td>
              <?php echo $artistaTemporal->id_art ?>
            </td>
            <td>
              <?php echo $artistaTemporal->nombre_art ?>
            </td>
            <td class="text-center">
              <a href="<?php echo site_url(); ?>/artistas/actualizar/<?php echo $artistaTemporal->id_art; ?>" title="Editar Artista">
                <button type="submit" name="button" class="btn btn-warning">
                  <i class="glyphicon glyphicon-pencil"></i>
                  Editar
                </button>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="<?php echo site_url(); ?>/artistas/borrar/<?php echo $artistaTemporal->id_art; ?>" title="Eliminar Artista" onclick="return confirm('¿Estás seguro de eliminar de forma permanente?');" style="color: red;">
                <button type="submit" name="button" class="btn btn-danger">
                  <i class="glyphicon glyphicon-trash"></i>
                  Eliminar
                </button>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <h1 class="text-center">No existen Artistas</h1>
<?php endif; ?>

<script type="text/javascript">
  $("#tbl_artistas").DataTable();
</script>
